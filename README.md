# stat_prog1

Repository for Statistical Programming 1

This repository is composed of the contents for the course Statistical Programming 1. 
It contains solutions for exercise 1-4 as R script files and for exercise 5 and beyond RMarkdown skripts.

Feel free to comment existing solutions of the exercises or propose better solutions! 

***

![](https://i.redd.it/ytitd72wz2b11.jpg)

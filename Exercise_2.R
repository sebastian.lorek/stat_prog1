#Exercise sheet 2
#Exercise 4.1
vec1 <- c(6,9,7,3,6,7,9,6,3,6,6,7,1,9,1)

#(i)
vec1==6

#(ii)
vec1>=6

#(iii)
vec1<6+2

#(iv)
vec1!=6

#(b)
vec2 <- vec1[-(1:3)]
ar <- array(vec2, dim=c(2,2,3))

#(i)
ar<=6/2+4

#(ii)
ar+2<=6/2+4

#(c)
diag(5)==0

#(d)
any(ar<=7)
all(ar<=7)
any(ar+2<=7)
all(ar+2<=7)

#(e)
A <- diag(5)==0
diag(A)
any(diag(A))

#Exercise 4.2
#(a)
foo <- c(7,1,7,10,5,9,10,3,10,2)
foo>5|foo==2

#(b)
bar <- c(8,8,4,4,5,1,5,6,6,8)
bar<6 & bar!=4

#(c)
baz <- foo+bar
baz

#(i)
baz[baz>=14&baz!=15]

#(ii)
baz2 <- baz/foo
baz2[baz2>4|baz2<=2]

#Exercise 4.3
#1
foo <- c(7,5,6,1,2,10,8,3,8,2)
#(a)
bar <- foo[foo>=5]
bar

#(b)
foo[foo<5]

#2
baz <- matrix(bar, nrow=2, ncol=3, byrow=TRUE)

#(a)
baz[baz==8] <- baz[1,2]^2
baz

#(b)
all(baz>4&baz<=25)

#3
#(a)
qux <- array(c(10,5,1,4,7,4,3,3,1,3,4,3,1,7,8,3,7,3),dim = c(3,2,3))
qux

#(a)
which(qux==3|qux==4, arr.ind=TRUE)

#(b)
qux[qux<3|qux>7] <- 100
qux

#4
#vector is automatically recycled to the full length of foo 
foo[c(FALSE,TRUE)]

#this works but this does not only use logical values per se
foo[(1:length(foo))%%2==0]
test <- seq(from=0, to=250, length.out = 25)
test
test[(1:length(test))%%2==0]

#Exercise 4.4
#1
s <- "The quick brown fox\n    jumped over\n        the lazydogs"
cat(s)

#2
num1 <- 4
num2 <- 0.75
paste("The result of multiplying", " ", num1, " ", "by", " ", num2 , " ", "is", " ", num1*num2, ".", sep="")

#3
mail <- "JoeThePlumber@uni-goettingen.de"
mail <- sub("JoeThePlumber", "firstname.lastname", mail)

#use stringr to create mail adresses but not asked in exercise
user_name <- "firstname.lastname"
stringr::str_interp("${user_name}@uni-goettingen.de")

#4
bar <- "How much wood could a woodchuck chuck"
#(a)
bar <- paste(bar,"if a woodchuck could chuck wood")
bar

#(b)
gsub("wood", "metal", bar)

#5
s <- "Two 6-packs for $12.99"

#(a)
substr(s,5,10)
substr(s,5,10)=="6-pack"

#(b)
#important to not include the $ sign
sub("12.99","10.99",s)

#Exercise 4.5
#1
sex <- c("F",rep("M",times=3),"F","F","F",rep("M",times=4),"F","M","F","F","F",rep("M",times=4))
#or safer 
sex <– rep("M",times=20)
sex[c(1,5:7,12,14:16)] <- "F"

party <- c("Labour",rep("National",times=2),"Labour","National","Greens","National",
           "National","Greens","Other","Greens","Labour","National","National","Labour","Labour",
           "National","National","Labour","Other")
#or safer 
party <- rep("National", times=20)
party[c(1,4,12,15,16,19)] <- "Labour"
party[c(6,9,11)] <- "Greens"
party[c(10,20)] <- "Other"

#2
fact1 <- factor(sex, levels=c("F","M"))
fact2 <- factor(party, levels=c("Labour","Greens","Maori","National","Other"))
# no ordering does not make sense as both factors contain only nominal information
# R seems to order the factors accordingly to their position in the alphabet when no levels argument is supplied otherwise it uses the order given in the levels argument
#3
#(a)
fact2[fact1=="M"]

#(b)
fact1[fact2=="National"]

#4
party <- c(party,c("National", "Maori", "Maori", "Labour", "Greens", "Labour"))
sex <- c(sex,c("M", "M", "F", "F", "F", "M"))
fact1 <- factor(sex, levels=levels(fact1))
fact2 <- factor(party, levels=levels(fact2))

#5
conf <- c(93, 55, 29, 100, 52, 84, 56, 0, 33, 52, 35, 53, 55, 46, 40, 40, 56, 45, 64, 31, 10, 29, 40, 95, 18, 61)
bps <- c(0,30,70,100)
fact3 <- cut(conf, breaks = bps, include.lowest = TRUE, labels = c("low","moderate","high"))
fact3

#6
fact3[fact2=="Labour"]
fact3[fact2=="National"]
# Voters that do vote for the Labour Party are more optimistic that their party wil
# win more seats than the National Party. There seems to be some systematic bias.

#Exercise 6.1
foo <- c(13563, -13156, -14319, 16981, 12921, 11979, 9568, 8833, -12968, 8133)
#(a)
foo <- foo^75
foo[is.finite(foo)]
#or
foo[foo<Inf&foo>-Inf]

#(b)
foo[foo!=-Inf]

#2
bar <- matrix(c(77875.40, 27551.45, 23764.30, -36478.88, -35466.25, -73333.85, 36599.69, -70585.69,-39803.81, 55976.34, 76694.82, 47032.00), ncol=4,nrow=3,byrow=TRUE)

#(a)
bar2 <- (bar^65)/Inf
which(is.nan(bar2),arr.ind=TRUE)

#(b)
bar3 <- bar^67+Inf
bar3[!is.nan(bar3)]
bar3[bar^67!=-Inf]

#Exercise 6.2
#1
foo <- c(4.3, 2.2, NULL, 2.4, NaN, 3.3, 3.1, NULL, 3.4, NA)
#(a)
length(foo)==8
# TRUE
#(b)
which(is.na(foo))
# FALSE, it returs the values 4 and 8
#(c)
is.null(foo)
#FALSE, it returns FALSE because the data type is numeric which is checked with the function
#(d)
foo[8] + 4/ NULL
#TRUE R seems to create an empty numeric vector of size 0
#null dominates the calculation but before a numeric vector is created

rm(list=ls())



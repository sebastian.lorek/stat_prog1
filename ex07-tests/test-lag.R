test_that("lagged", {
  x <- 1:10
  n <- 3
  x_lagged <- my_lag(x, n)
  expect_equal(x_lagged, c(NA, NA, NA, 1:7))
})

test_that("n > length(x)", {
  x <- 1:2
  n <- 3
  x_lagged <- my_lag(x, n)
  expect_equal(x_lagged, c(NA, NA))
})

test_that("n == 0", {
  x <- 1:3
  n <- 0
  x_lagged <- my_lag(x, n)
  expect_equal(x_lagged, 1:3)
})

test_that("x is non scalar", {
  x <- list(1:3, "hello world")
  n <- 1
  expect_error(my_lag(x, n))
})
